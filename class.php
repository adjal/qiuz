<?php

spl_autoload_register(function ($class) {
	if(file_exists('classes/' . $class . '.class.php')) {
		require_once 'classes/' . $class . '.class.php';
		return true;
	}
	return false;
});