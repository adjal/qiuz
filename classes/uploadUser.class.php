<?php
class uploadUser {

    private $table = 'imported_users';

    public function add() {
        $a = explode('.', $_FILES['userfile']['name']);
        $a = array_pop($a);

        // обновляем список импорта
        // new == 1, показываем в системе
        orm::Update($this->table, array("new"=>"0"), array("new"=>'1'));

        if(!empty($_FILES['userfile']['tmp_name']) && $a === 'txt') {
            $file = file($_FILES['userfile']['tmp_name']);

            $result = '
            <h5>Результат добавления. Сохраните полученный результат.</h5>
            <table border="1">
            <tr>
                <th>ФИО</td>
                <th>Логин</td>
                <th>Пароль</td>
            </tr>
            ';

            foreach ($file as $value) {
                $name = explode(' ', $value);
                $temp = $name[0] . '-' . $name[1];
                $lat = $this->rus2translit($temp);
                $password = $this->generate_password(6);

                $lat = $this->nameUser($lat);

                orm::Insert("imported_users", array(
                                            "name"=>$name[1],
                                            "surname"=>$name[0],
                                            "user_name"=>$lat, // логин
                                            "password"=>md5($password),
                                            "new"=>1 // указываем что новый пользователь
                                        ));

                $result .= '<tr><td>' . $value . '</td> <td>' . $lat . '</td> <td>' . $password . '</td></tr>';

            }

            $result .= '</table><br>';
        } else {
            $result = 'Ошибка. Загрузите txt файл.';
        }
        return $result;
    }

    // проверка пользователя
    // если такой пользователь уже имеется, то возвращает 1, иначе 0
    public function checkUser($user) {
        $results = orm::Select("imported_users", array(), array("user_name"=>$user) , "");
        return db::num_rows($results);
    }

    // если пользователь с таким логинем уже имеется, то генерирует новый логин
    // если же нет, возращает то что пришло
    public function nameUser($name) {
        $user = $this->checkUser($name);

        while($user) {
            $name .= rand(0, 9);
            $user = $this->checkUser($name);
        }

        return $name;
    }

    // генератор случайных паролей
    // метод принимает длину пароля
    // $arr какие символы можно использовать
    public function generate_password($number) {
        $arr = array('a','b','c','d','e','f',
                     'g','h','i','j','k','l',
                     'm','n','o','p','r','s',
                     't','u','v','x','y','z',
                     '1','2','3','4','5','6',
                     '7','8','9','0');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
          // Вычисляем случайный индекс массива
          $index = rand(0, count($arr) - 1);
          $pass .= $arr[$index];
        }
        return $pass;
    }

    public function rus2translit($string)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => "",  'ы' => 'y',   'ъ' => "",
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => "",  'Ы' => 'Y',   'Ъ' => "",
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        $string = preg_replace('/[^A-Za-zА-Яа-я0-9-.]/u', '', $string);
        $string = strtr($string, $converter);
        $string = mb_strtolower($string);
        return $string;
    }
}