<?php

  /*
		This code has been developed by Els . Email : elshanb@gmail.com

		Say, "He is Allah , [who is] One,
		Allah , the Eternal, Absolute.
		He neither begets nor is born,
		Nor is there to Him any equivalent."
		(112 - Al-'Ikhlas)

   */

  define("SQL_IP", "localhost"); // ip address of mysql database
  define("SQL_USER", "root");  // username for connecting to mysql
  define("SQL_PWD","12345"); // password for connecting to mysql
  define("SQL_DATABASE","quiz2"); // database where you have executed sql scripts

  define("WEB_SITE_URL","http://quiz/"); // the url where you installed this script . do not delete last slash
  define("USE_MATH", "no"); // yes , no . if you want to use math symbols , you have to enable it
  define("DEBUG_SQL","no"); // enable it , if you want to view sql queries .
  define("PAGING","30");  // paging for all grids
  define("SHOW_MENU","registered"); // all = all users , registered = registered users  , nobody = menu will be disabled
  define("SHOW_MENU_ON_LOGIN_PAGE", "no"); // yes , no

  define("SITE_TEMPLATE", "standard"); // gold  , standard

  define("MAIL_FROM", ""); //
  define("MAIL_CHARSET", "UTF-8");
  define("MAIL_USE_SMTP", "yes");
  define("MAIL_SERVER", ""); // only if smtp enabled
  define("MAIL_USER_NAME", ""); // only if smtp enabled
  define("MAIL_PASSWORD", ""); // only if smtp enabled
  define("MAIL_PORT", "25"); // only if smtp enabled

  // Documentation : http://phpexamscript.net/php_exam_script_forum/
  define("PAYPAL_LOGIN","no"); // yes , no . If you want to enable Login with paypal , turn it to "yes"
  define("PAYPAL_SCOPE","openid email profile"); // do not change
  define("PAYPAL_CLIENTID",""); // Client ID of Paypal application
  define("PAYPAL_SECRET",""); // Secret code of Paypal application
  define("PAYPAL_LOGIN_USE_SANDBOX","yes"); // yes , no . After testing , turn it to "no"
  define("PAYPAL_SHOW_ERROR","yes"); // yes , no . Displays error that comes from Paypal
  define("PAYPAL_LOGIN_ONLY_PAID","yes"); // yes , no . Turn it to "yes" if you want to restrict access only to paid users , and turn it to no if you want all paypal users to be able to login
  define("PAYPAL_ERROR","You need to pay before logining in");

  define("PAYPAL_ENABLED","no"); //  yes , no . Turn it to yes if you want to receive payments from paypal
  define("PAYPAL_SHOW_BUY_BUTTON","no"); // yes , no . Shows "Buy now" button on login page
  define("PAYPAL_SELLER_EMAIL",""); // enter your business e-mail of your paypal account
  define("PAYPAL_NOTIFY_SUCCESS_PAYMENT","no"); // yes , no . sends e-mail on success payment : but you need to configurate your mail settings
  define("PAYPAL_NOTIFY_FAIL_PAYMENT","no"); // yes , no . sends e-mail on success payment : but you need to configurate your mail settings
  define("PAYPAL_ACCEPT_AMOUNT","100"); // amount that you want to receive for the login . if amount will be less than
  define("PAYPAL_CURRENCY","USD"); // currency in which you want to receive money
  define("PAYPAL_DATA_NAME","Quiz payment"); // service name
  define("PAYPAL_USE_SANDBOX","yes"); // yes , no . After testing , turn it to "no"

  $PAGE_HEADER_TEXT= "Quizzes and Surveys"; //only in gold and green templates
  define("HEADER_TEXT_LINK", "http://quiz/");
  $PAGE_SUB_HEADER_TEXT="Web based quiz software allowing to create quizzes and surveys"; //only in gold and green templates
  $PAGE_FOOTER_TEXT="Contact mail :"; //only in gold and green templates

  function Imported_Users_Password_Hash($entered_password,$password_from_db)
  {
      return md5($entered_password);
  }

  @session_start();

  // LANGUAGE CONFIGURATION

  // if you translate this script to another language , please send copy of translated file to me also : elshanb@gmail.com . Thanks :-)
  $LANGUAGES = array("english.php"=>"English","russian.php"=>"Russian","dutch.php"=>"Dutch");
  $DEFAULT_LANGUAGE_FILE="english.php";

  //----------------------------do not touch the code below--------------------------------

  if(isset($_SESSION['lang_file']))
  {
      $DEFAULT_LANGUAGE_FILE = $_SESSION['lang_file'];
  }

  if(isset($_GET['lang']))
  {
      $lang_arr = util::translate_array($LANGUAGES);
      if(isset($lang_arr[$_GET['lang']])) $DEFAULT_LANGUAGE_FILE = $lang_arr[$_GET['lang']];
  }

  require "lang/".$DEFAULT_LANGUAGE_FILE;

  ini_set ('magic_quotes_gpc', 0);
  ini_set ('magic_quotes_runtime', 0);
  ini_set ('magic_quotes_sybase', 0);
  ini_set('session.bug_compat_42',0);
  ini_set('session.bug_compat_warn',0);

  error_reporting(E_ERROR | E_WARNING | E_PARSE);

  //----------------------------------------------------------------------


  /*

		Visit our web site for documentation and for other versions

		http://aspnetpower.com/elsphpwebquiz/index.php?module=online_demo
		http://phpexamscript.net


  */

?>
