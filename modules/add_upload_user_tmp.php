<?php if(!isset($RUN)) { exit(); } ?>

<?php if(empty($result))  {?>
<form method="post" name="form1" enctype="multipart/form-data">

<div class="desc_text">
    <div class="field">
        <p>Выберите Ваш файл для загрузки:
        <input type="file" id="userfile" name="userfile"/></p>
    </div>
    <div class="field">
        <p>Загружаемый файл должен быть <b>txt</b> формата в кодировке <b>UTF-8</b>. При сохранении файлы выберите "Сохранить как..." и укажите кодировку <b>UTF-8</b>.</p>
        <p>Один пользователь пишется в одну строку в виде: "Фамилия Имя"</p>
        <p>После загрузки вы получите список : ФИО, логин, пароль. Обязательно сохраните полученный список, иначе узнать пароли пользователей нельзя.</p>
    </div>
    <div class="field">
            <input class="st_button" type="submit" name="btnSave" value="<?php echo IMPORT ?>" id="btnSave" onclick="return checkform();" />
            <input class="st_button" type="button" name="btnCancel" value="<?php echo CANCEL ?>" id="btnCancel" onclick="javascript:window.location.href='?module=local_users'" />
    </dib>
    <input type="hidden" id="hdnMode" value="<?php echo $mode ?>">
</div>
</form>
<?php
} else {
    echo $result;
}


?>